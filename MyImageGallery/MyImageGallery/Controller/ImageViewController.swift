//
//  ImageViewController.swift
//  MyImageGallery
//
//  Created by Dima Shelkov on 6/18/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIScrollViewDelegate {
    
    var imageView = UIImageView()
    private var autoZoomed = true
    
    var imageURL: URL? {
        didSet {
            image = nil
            if view.window != nil {
                fetchImage()
            }
        }
    }
    
    private var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
            scrollView?.zoomScale = 1.0
            let size = image?.size ?? CGSize.zero
            scrollView?.contentSize = size
            imageView.sizeToFit()
            spinner?.stopAnimating()
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 1/2
            scrollView.maximumZoomScale = 1.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if imageView.image == nil {
            fetchImage()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        zoomScaleToFit()
    }
    
    @IBAction func doneButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    private func fetchImage() {
        spinner.startAnimating()
        if let url = imageURL {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                let urlContents = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    if let imageData = urlContents, url == self?.imageURL {
                        self?.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }
    
    private func zoomScaleToFit() {
        if !autoZoomed { return }
        
        guard let sv = scrollView,
            image != nil && (imageView.bounds.size.width > 0) && (scrollView.bounds.size.width > 0) else { return }
        
        let widthRatio = scrollView.bounds.size.width / imageView.bounds.size.width
        let heightRatio = scrollView.bounds.size.height / self.imageView.bounds.size.height
        sv.zoomScale = (widthRatio > heightRatio) ? widthRatio : heightRatio
        sv.contentOffset = CGPoint(x: (imageView.frame.size.width - sv.frame.size.width) / 2,
                                   y: (imageView.frame.size.height - sv.frame.size.height) / 2)
    }
}
