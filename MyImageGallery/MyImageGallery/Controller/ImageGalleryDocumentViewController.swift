//
//  DocumentViewController.swift
//  MyImageGallery
//
//  Created by Dima Shelkov on 6/14/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ImageGalleryDocumentViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDragDelegate, UICollectionViewDropDelegate {
    
    var document: ImageGalleryDocument?
    var imageGallery = ImageGallery()
    var defaultWidth: CGFloat = 250
    
    @IBOutlet weak var imagesCollectionView: UICollectionView! {
        didSet {
            imagesCollectionView.dataSource = self
            imagesCollectionView.delegate = self
            imagesCollectionView.dragDelegate = self
            imagesCollectionView.dropDelegate = self
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imagesCollectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        document?.open { success in
            guard success else { return }
            self.title = self.document?.localizedName
            self.imageGallery = self.document?.imageGallery ?? ImageGallery()
            self.imagesCollectionView.reloadData()
        }
    }
    
    @IBAction func save(_ sender: UIBarButtonItem? = nil) {
        document?.imageGallery = imageGallery
        guard document?.imageGallery != nil else { return }
        document?.updateChangeCount(.done) 
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        save()
        dismiss(animated: true) {
            self.document?.close()
        }
    }
    
    func toImageVC(with url: URL?) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let documentVC = storyBoard.instantiateViewController(withIdentifier: "ImageVC")
        
        guard let imageVC = documentVC.contents as? ImageViewController else { return }
        imageVC.imageURL = url
        present(documentVC, animated: true)
    }
    
    private func addTapGestureRecognizer(for cell: MyImageCollectionViewCell) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCard(recognizedBy: )))
        cell.addGestureRecognizer(tap)
    }
    
    @objc private func tapOnCard(recognizedBy recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state,
            let cell = recognizer.view as? MyImageCollectionViewCell else { return }
        toImageVC(with: cell.imageURL)
    }
    
    @IBAction func dismissDocumentViewController() {
        dismiss(animated: true) {
            self.document?.close(completionHandler: nil)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return imageGallery.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                let imageInfo = imageGallery.images[sourceIndexPath.item]
                collectionView.performBatchUpdates({
                    imageGallery.images.remove(at: sourceIndexPath.item)
                    imageGallery.images.insert(imageInfo, at: destinationIndexPath.item)
                    collectionView.deleteItems(at: [sourceIndexPath])
                    collectionView.insertItems(at: [destinationIndexPath])
                })
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
            } else {
                let placeholderContext = coordinator.drop(item.dragItem, to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "Image Cell"))
                
                var imageURLLocal: URL?
                var aspectRatioLocal: Double?
                
                item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { (provider, error) in
                    DispatchQueue.main.async {
                        if let image = provider as? UIImage {
                            aspectRatioLocal = Double(image.size.width / image.size.height)
                        }
                    }
                }
                
                item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (provider, error) in
                    DispatchQueue.main.async {
                        guard let url = provider as? URL else { return }
                        imageURLLocal = url.imageURL
                        
                        
                        if imageURLLocal != nil, aspectRatioLocal != nil {
                            placeholderContext.commitInsertion(dataSourceUpdates: { insertionIndexPath in
                                self.imageGallery.images.insert(ImageModel( url: imageURLLocal!, aspectRatio: aspectRatioLocal!),
                                                                at: insertionIndexPath.item) })
                        } else {
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
            }
        }
        save()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image Cell", for: indexPath)
        cell.backgroundColor = .clear
        
        if let imageCell = cell as? MyImageCollectionViewCell {
            imageCell.imageURL = imageGallery.images[indexPath.item].url
            addTapGestureRecognizer(for: imageCell)
        }
        return cell
    }
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard let itemCell = imagesCollectionView.cellForItem(at: indexPath) as? MyImageCollectionViewCell,
            let image = itemCell.image.image else { return [] }
        
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
        dragItem.localObject = imageGallery.images[indexPath.item]
        return [dragItem]
    }
}

extension ImageGalleryDocumentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = defaultWidth
        let aspectRatio = CGFloat(imageGallery.images[indexPath.item].aspectRatio)
        return CGSize(width: width, height: width / aspectRatio)
    }
}
