//
//  MyImageCollectionViewCell.swift
//  MyImageGallery
//
//  Created by Dima Shelkov on 6/17/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit
import SDWebImage

class MyImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var image: UIImageView! {
        didSet {
            spinner?.stopAnimating()
        }
    }
    
    var imageURL: URL? {
        didSet {
            spinner?.startAnimating()
            saveImage()
        }
    }
    
    private func saveImage() {
        image.sd_setImage(with: imageURL)
    }
}
