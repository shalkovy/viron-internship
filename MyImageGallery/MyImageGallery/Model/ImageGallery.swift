//
//  ImageGallery.swift
//  MyImageGallery
//
//  Created by Dima Shelkov on 6/15/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

// TODO: - write model for image and gallery
struct ImageModel: Codable {
    var url: URL
    var aspectRatio: Double
}

struct ImageGallery: Codable {
    var images = [ImageModel]()
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init?(json: Data) {
        guard let newValue = try? JSONDecoder().decode(ImageGallery.self, from: json) else { return nil }
        self = newValue
    }
    
    init() {
        self.images = [ImageModel]()
    }
    
    subscript(index: Int) -> ImageModel {
        return images[index]
    }
}
