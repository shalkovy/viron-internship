//
//  ImageGalleryDocument.swift
//  MyImageGallery
//
//  Created by Dima Shelkov on 6/14/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ImageGalleryDocument: UIDocument {
    
    var imageGallery: ImageGallery?
    
    override func contents(forType typeName: String) throws -> Any {
        return imageGallery?.json ?? Data()
    }
    
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        guard let json = contents as? Data else { return }
        imageGallery = ImageGallery(json: json)
    }
}

