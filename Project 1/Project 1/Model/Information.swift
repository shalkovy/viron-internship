//
//  CustomView.swift
//  Project 1
//
//  Created by Dima Shelkov on 4/10/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

struct Information {

    let nameAndSecondName = "Name : Dima \nSecond Name : Shelkov"
    let aboutMe = "Age : 23 \nFavourite film : Lord of The Rings"
    let forMoreInformation = "Shake your phone to get more information"
    let toGetBack = "Shake your phone to return"
    
    let anotherNameAndSecond = "Name : Jesus \nSecond Name : Christ \nAge : 2019"
//    let aboutSmb = "Age : 2019 \nFavourite film : Lord of The Rings"
//    let forMoreInformation = "Shake your phone to get more information"
//    let toGetBack = "Shake your phone to return"
}
