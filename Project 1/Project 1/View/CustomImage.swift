//
//  CustomImage.swift
//  Project 1
//
//  Created by Dima Shelkov on 4/10/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class CustomImage: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        updateImage()
    }
    
    func updateImage() {
        self.contentMode = .scaleAspectFill
        let radius = self.frame.height / 2
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
