//
//  ViewController.swift
//  Project 1
//
//  Created by Dima Shelkov on 4/9/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class AboutMeVC: UIViewController {

    let information = Information()
    var isNameTrue = true
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var shakeTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load text
        self.informationLabel.text = information.nameAndSecondName
        self.shakeTextLabel.text = information.forMoreInformation
    }

    // Change text after shake
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        changeText()
        isNameTrue = !isNameTrue
    }
    
    // Change texts
    func changeText() {
        informationLabel.text = isNameTrue ? information.aboutMe : information.nameAndSecondName
        shakeTextLabel.text = isNameTrue ? information.toGetBack : information.forMoreInformation
    }

}
