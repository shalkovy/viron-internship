//
//  AboutSomeoneVC.swift
//  Project 1
//
//  Created by Dima Shelkov on 4/16/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit
import SnapKit

class AboutSomeoneVC: UIViewController {
    
    let customImage = CustomImage()
    let text = Information()
    let topView = UIView()
    let bottomView = UIView()
    var center: Int = 0

    let centerLable: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 3
        label.font = UIFont(name: "Helvetica", size: 26)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        center = Int(self.view.frame.size.height/2)
        customImage.image = UIImage(named: "jesus")
        centerLable.text = text.anotherNameAndSecond
        
        setupMainView()
        setupBottomView()
        setupTopView()
        setupAvatarImageView()
        setupInformationLabel()
    }
   
    func setupMainView() {
        view.backgroundColor = .lightGray
    }
    
    func setupTopView() {
        self.view.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.topMargin.right.left.equalTo(0)
            make.bottomMargin.equalTo(super.view.snp_centerY)
        }
    }
    
    func setupBottomView() {
        self.view.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.bottomMargin.right.left.equalTo(0)
            make.topMargin.equalTo(super.view.snp_centerY)
        }
    }
    
    func setupAvatarImageView() {
        topView.addSubview(customImage)
        customImage.snp.makeConstraints { (make) in
            make.bottom.equalTo(-10)
            make.left.greaterThanOrEqualTo(25)
            make.height.equalTo(customImage.snp.width)
            make.centerXWithinMargins.equalTo(topView.center.x)
            make.centerYWithinMargins.equalTo(topView.center.y)
        }
    }
    
    func setupInformationLabel() {
        self.bottomView.addSubview(centerLable)
        centerLable.snp.makeConstraints { (make) in
            make.centerXWithinMargins.equalTo(bottomView.center.x)
            make.centerYWithinMargins.equalTo(bottomView.center.y)
        }
    }
}
