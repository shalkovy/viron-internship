//
//  ConcentrationThemeChooserViewController.swift
//  Concentration
//
//  Created by Dima Shelkov on 5/21/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController {
    
    let themes = [
        "Halloween" : "🦇😱🎃👻😈🙀🍬🍭🍎👺👾",
        "Animals" : "🦓🦒🦔🦗🦝🦙🦛🦘🦡🦚🦢",
        "Sports" : "🏊‍♀️🏄‍♀️🤽‍♀️🚴‍♀️⛹️‍♀️🚣‍♀️🚵‍♀️🤾‍♀️🏊🏻‍♂️🚴‍♂️🤾‍♂️"
    ]
    
    @IBAction func changeTheme(_ sender: Any) {
        performSegue(withIdentifier: "Choose Theme", sender: sender)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Choose Theme" {
            if let button = sender as? UIButton {
                if let themeName = button.currentTitle, let theme = themes[themeName] {
                    if let cvc = segue.destination as? ConcentrationViewController {
                        cvc.theme = theme
                    }
                }
            }
        }
    }
 

}
