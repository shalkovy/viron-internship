//
//  EmojiThemes.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/23/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class EmojiThemes {
    
    let emojiArray: [[Any]] = [
        [#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), "🦇", "😱", "🎃", "👻", "😈", "🙀", "🍬", "🍭", "🍎", "👺", "👾"],
        [#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.4508578777, green: 0.9882974029, blue: 0.8376303315, alpha: 1), "🦓", "🦒", "🦔", "🦗", "🦝", "🦙", "🦛", "🦘", "🦡", "🦚", "🦢"],
        [#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1), "🏊‍♀️", "🏄‍♀️", "🤽‍♀️", "🚴‍♀️", "⛹️‍♀️", "🚣‍♀️", "🚵‍♀️", "🤾‍♀️", "🏊🏻‍♂️", "🚴‍♂️", "🤾‍♂️"],
        [#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), "🍏", "🍎", "🍐", "🍊", "🍋", "🍌", "🍉", "🍇", "🍓", "🍈", "🍒"],
        [#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), "🏳️‍🌈", "🇦🇲", "🇧🇾", "🇧🇦", "🇧🇴", "🇧🇷", "🇨🇦", "🇰🇾", "🇨🇮", "🇩🇪", "🇬🇪"],
        [#colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1), "🚗", "🚕", "🚙", "🚌", "🚑", "🚓", "🏎", "🚎", "🚒", "🚐", "🚚"]
    ]
    
    func returnEmojiArray() -> [Any] {
        let randomIndex = Int.random(in: 0..<emojiArray.count)
        return emojiArray[randomIndex]
    }
    
}
