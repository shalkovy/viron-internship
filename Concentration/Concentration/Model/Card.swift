//
//  Card.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/22/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

struct Card: Equatable {

    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    var isWatched = false
    static var identifierFactory = 0
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
}
