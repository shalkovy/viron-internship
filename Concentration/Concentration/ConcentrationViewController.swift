//
//  ConcentrationViewController.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/20/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ConcentrationViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    var emojiChoises = ""
    var emoji = [Card: String]()
    var buttonColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
    
    var theme: String? {
        didSet {
            emojiChoises = theme ?? ""
            emoji = [:]
            updateViewFromModel()
        }
    }
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newGame()
        updateViewFromModel()
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        game.flips += 1
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    @IBAction private func newGameButtonPressed(_ sender: Any) {
        newGame()
        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        guard cardButtons != nil else { return }
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = .white
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? .clear : buttonColor
            }
        }
        scoreLabel.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flips)"
    }

    private func emoji(for card: Card) -> String {
        if emoji[card] == nil, emojiChoises.count > 0 {
            let randomStringIndex = emojiChoises.index(emojiChoises.startIndex, offsetBy: 1)
            emoji[card] = String(emojiChoises.remove(at: randomStringIndex))
        }
        return emoji[card] ?? "?"
    }

    
    private func newGame() {
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
//        let emojiArray = EmojiThemes().returnEmojiArray()
//        var colorThemesArray = EmojiThemes().themeColorsArray
//        guard
//            let cardsColor = emojiArray.removeFirst() as? UIColor,
//            let backgroundColor = emojiArray.removeFirst() as? UIColor,
//            let emojiChoises = emojiArray
//            else { return }
//        self.buttonColor = cardsColor
//        self.view.backgroundColor = backgroundColor
        guard let theme = theme else { return }
        self.emojiChoises = theme
        updateViewFromModel()
    }
}

