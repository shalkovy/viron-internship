//
//  ViewController.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/20/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    var emojiChoises = [String]()
    var emoji = [Int: String]()
    var buttonColor = UIColor()
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newGame()
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        game.flips += 1
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    @IBAction func newGameButtonPressed(_ sender: Any) {
        newGame()
        updateViewFromModel()
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = .white
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? .clear : buttonColor
            }
        }
        scoreLabel.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flips)"
    }

    func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoises.count > 0 {
            let randomIndex = Int.random(in: 0..<emojiChoises.count)
            emoji[card.identifier] = emojiChoises.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }

    func newGame() {
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
        var emojiArray = EmojiThemes().returnEmojiArray()
        guard
            let cardsColor = emojiArray.removeFirst() as? UIColor,
            let backgroundColor = emojiArray.removeFirst() as? UIColor,
            let emojiChoises = emojiArray as? [String]
            else { return }
        self.buttonColor = cardsColor
        self.view.backgroundColor = backgroundColor
        self.emojiChoises = emojiChoises
        updateViewFromModel()
    }
}

