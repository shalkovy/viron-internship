//
//  File.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

class SetDeck {
    
    var generatedArrayOfCards = [SetCard]()
    
    init() {
        generatedArrayOfCards = generateDeck()
    }
    
    private func generateDeck() -> [SetCard] {
        for color in SetCard.Color.allCases {
            for shape in SetCard.Shape.allCases {
                for number in SetCard.Number.allCases {
                    for shading in SetCard.Shading.allCases {
                        let card = SetCard(color: color, shape: shape, numberOfElements: number, shading: shading)
                        generatedArrayOfCards.append(card)
                    }
                }
            }
        }
        return generatedArrayOfCards.shuffled()
    }
}


