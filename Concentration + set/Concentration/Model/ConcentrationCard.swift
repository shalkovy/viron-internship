//
//  ConcentrationCard.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/22/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

struct ConcentrationCard: Hashable {

    var isFaceUp = false
    var isMatched = false
    var isWatched = false
    private var identifier: Int
    private static var identifierFactory = 0
    
    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    static func == (lhs: ConcentrationCard, rhs: ConcentrationCard) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
