//
//  Concentration.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/22/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

class Concentration {
    
    var score = 0
    var flips = 0
    var cards = [ConcentrationCard]()
    var indexOfOneAndOnlyFaceUpCard: Int?
    
    init(numberOfPairsOfCards: Int) {
        for _ in 1...numberOfPairsOfCards {
            let card = ConcentrationCard()
            cards += [card, card]
        }
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        guard !cards[index].isMatched else { return }
        guard let matchIndex = indexOfOneAndOnlyFaceUpCard else {
            for flipDownIndex in cards.indices {
                cards[flipDownIndex].isFaceUp = false
            }
            cards[index].isFaceUp = true
            indexOfOneAndOnlyFaceUpCard = index
            return
        }
        if matchIndex != index {
            isCardWatched(firstIndex: index, secondIndex: matchIndex)
            isCardMatched(firstIndex: index, secondIndex: matchIndex)
            cards[index].isFaceUp = true
            indexOfOneAndOnlyFaceUpCard = nil
        }
    }
    
    // Check if cards watch
    func isCardWatched(firstIndex: Int, secondIndex: Int) {
        if cards[firstIndex] == cards[secondIndex] {
            score += 2
        } else {
            if cards[secondIndex].isWatched { score -= 1 }
            if cards[firstIndex].isWatched { score -= 1 }
        }
        cards[firstIndex].isWatched = true
        cards[secondIndex].isWatched = true
    }
    
     // Check if cards match
    func isCardMatched(firstIndex: Int, secondIndex: Int) {
        if cards[firstIndex] == cards[secondIndex] {
            // Matching cards
            cards[secondIndex].isMatched = true
            cards[firstIndex].isMatched = true
        }
    }
}
