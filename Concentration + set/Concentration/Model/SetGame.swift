//
//  Set.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

class SetGame {
    
    var mainDeck = SetDeck().generatedArrayOfCards
    
    var cardsOnScreen = [SetCard]()
    var cardsSelected = [SetCard]()
    var cardsTryMatched = [SetCard]()
    var cardsRemoved = [SetCard]()
    
    var isSet: Bool? {
        get {
            guard cardsTryMatched.count == 3 else { return nil }
            return checkingForSetArray()
        }
        set {
            if newValue != nil {
                cardsTryMatched = cardsSelected
                cardsSelected.removeAll()
            } else {
                cardsTryMatched.removeAll()
            }
        }
    }
    
    func dealThreeCards() -> [SetCard]? {
        var cards = [SetCard]()
        guard !mainDeck.isEmpty else { return cards }
        for _ in 0..<3 {
            cards += [mainDeck.removeFirst()]
        }
        cardsOnScreen += cards
        return cards
    }
    
    func generateFirstDeck() {
        for _ in 0..<12 {
            cardsOnScreen.append(mainDeck.removeFirst())
        }
    }
    
    // MARK: - touchButton Method
    func selectCard(addedIndex index: Int) {
        let cardChoosen = cardsOnScreen[index]
        if !cardsRemoved.contains(cardChoosen) && !cardsTryMatched.contains(cardChoosen){
            if  isSet != nil{
                if isSet! { removeCards()}
                isSet = nil
            }
            if cardsSelected.count == 2, !cardsSelected.contains(cardChoosen){
                cardsSelected += [cardChoosen]
                isSet = checkingForSetArray()
            } else {
                cardsSelected.inOut(element: cardChoosen)
            }
        }
    }
    
    func removeCards() {
        if cardsOnScreen.count == 12, let replaceCards = dealThreeCards() {
            cardsOnScreen.replace(elements: cardsTryMatched, with: replaceCards)
        } else {
            cardsOnScreen.remove(elements: cardsTryMatched)
        }
        cardsRemoved += cardsTryMatched
        cardsTryMatched.removeAll()
    }
    
    func findSet() -> [SetCard] {
        var arrayOfCards = [SetCard]()
        for firstCard in cardsOnScreen {
            for secondCard in cardsOnScreen {
                for thirdCard in cardsOnScreen {
                    if firstCard != secondCard && secondCard != thirdCard  && firstCard != thirdCard {
                        arrayOfCards.append(firstCard)
                        arrayOfCards.append(secondCard)
                        arrayOfCards.append(thirdCard)
                    }
                    cardsTryMatched = arrayOfCards
                    if checkingForSetArray() {
                        arrayOfCards = cardsTryMatched
                        cardsTryMatched.removeAll()
                        return arrayOfCards
                    } else {
                        arrayOfCards.removeAll()
                    }
                }
            }
        }
        return arrayOfCards
    }
    
    // MARK: - Checking for set
    func checkingForSetArray() -> Bool {
        guard cardsTryMatched.count == 3 else { return false }
        let cardOne = cardsTryMatched[0], cardTwo = cardsTryMatched[1], cardThree = cardsTryMatched[2]
        if  formMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) &&
            colorMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) &&
            numberMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) &&
            textureMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) {
            return true
        }
        return false
    }
    
    // MARK: - Matched or doesn't matched functions
    private func numberMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.numberOfElements == secondCard.numberOfElements && secondCard.numberOfElements == thirdCard.numberOfElements && firstCard.numberOfElements == thirdCard.numberOfElements {
            return true
        } else if firstCard.numberOfElements != secondCard.numberOfElements && secondCard.numberOfElements != thirdCard.numberOfElements && firstCard.numberOfElements != thirdCard.numberOfElements {
            return true
        }
        return false
    }
    
    private func colorMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.color == secondCard.color && secondCard.color == thirdCard.color && firstCard.color == thirdCard.color {
            return true
        } else if firstCard.color != secondCard.color && secondCard.color != thirdCard.color && firstCard.color != thirdCard.color {
            return true
        }
        return false
    }
    
    private func textureMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.shading == secondCard.shading && secondCard.shading == thirdCard.shading && firstCard.shading == thirdCard.shading {
            return true
        } else if firstCard.shading != secondCard.shading && secondCard.shading != thirdCard.shading && firstCard.shading != thirdCard.shading {
            return true
        }
        return false
    }
    
    private func formMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.shape == secondCard.shape && secondCard.shape == thirdCard.shape && firstCard.shape == thirdCard.shape {
            return true
        } else if firstCard.shape != secondCard.shape && secondCard.shape != thirdCard.shape && firstCard.shape != thirdCard.shape {
            return true
        }
        return false
    }
}
