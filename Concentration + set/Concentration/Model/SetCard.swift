//
//  SetCard.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

struct SetCard: Equatable {
    
    enum Color: String, CaseIterable {
        case red = "red"
        case purple = "purple"
        case green = "green"
    }
    
    enum Shape: String, CaseIterable {
        case oval = "oval"
        case squiggle = "squiggle"
        case diamond = "diamond"
    }
    
    enum Number: Int, CaseIterable {
        case one = 1
        case two = 2
        case three = 3
    }
    
    enum Shading: String, CaseIterable {
        case solid = "solid"
        case striped = "striped"
        case outlined = "outlined"
    }
    
    let color: Color
    let shape: Shape
    let numberOfElements: Number
    let shading: Shading
}
