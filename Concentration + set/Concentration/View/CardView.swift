//
//  CardView.swift
//  setGame
//
//  Created by Dima Shelkov on 5/15/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class CardView: UIView {
    
    enum Shading {
        case solid
        case striped
        case outlined
    }
    
    enum Shape {
        case squiggle
        case oval
        case diamond
    }
    
    private struct SizeRatio {
        static let cornerRadiusToBoundsHeight: CGFloat = 0.06
    }
    
    var isMatched: Bool? {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }

    var shape: Shape = .oval {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var shading: Shading = .solid {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var color = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)  {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var count: Int = 1 {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var isSelected = false {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var isRounded = false {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    private var cornerRadius: CGFloat {
        return bounds.size.height * SizeRatio.cornerRadiusToBoundsHeight
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setNeedsDisplay()
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func draw(_ rect: CGRect) {
        isOpaque = false
        let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).setFill()
        roundedRect.fill()
        
        func roundCard() {
            layer.borderWidth = 3.0
            layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        }
        
        func cleanCard() {
            layer.borderWidth = 1.0
            layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        }
        
        isRounded == true ? roundCard() : cleanCard()
        
        if isSelected {
            #colorLiteral(red: 0.8321695924, green: 0.985483706, blue: 0.4733308554, alpha: 1).setFill()
        }
        roundedRect.fill()
        
        drawShapes(shape, count: count)
        
        layer.backgroundColor = UIColor.white.cgColor
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    //MARK: - Animations
    
    // Dealing Cards animation
    func animateDeal(from deckCenter: CGPoint, delay: TimeInterval) {
        let currentCenter = center
        let currentBounds = bounds
        
        center =  deckCenter
        alpha = 1
        bounds = CGRect(x: 0.0, y: 0.0, width: 0.6 * bounds.width,
                        height: 0.6 * bounds.height)
        UIViewPropertyAnimator.runningPropertyAnimator (
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = currentCenter
                self.bounds = currentBounds
            },
            completion: { position in
                UIView.transition(
                    with: self,
                    duration: 0.3,
                    options: [],
                    animations: {})
            })
    }
    
    // Fly animations
    var addDiscardPile : (() -> Void)?
    
    func animateFly(to discardPileCenter: CGPoint, delay: TimeInterval) {
        UIViewPropertyAnimator.runningPropertyAnimator (
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = discardPileCenter
            },
            completion: { _ in
                UIView.transition(
                    with: self,
                    duration: 0.75,
                    options: [.transitionFlipFromLeft],
                    animations: {
                        self.transform = CGAffineTransform.identity
                            .rotated(by: CGFloat.pi / 2.0)
                        self.bounds = CGRect(x: 0.0, y: 0.0,
                                             width: 0.7 * self.bounds.width,
                                             height: 0.7 * self.bounds.height)
                },
                    completion: { _ in
                        self.addDiscardPile?()
                    })
        })
    }
    
    //MARK: - Drawing elements on cards functions
    private func drawShapes(_ shape: Shape, count: Int) {
        let paths = drawNumberOfPaths(count: count)
        drawPaths(for: paths)
    }
    
    private func drawNumberOfPaths(count: Int) -> [UIBezierPath] {
        let size = CGFloat(bounds.maxX / 1.6)
        let spacing = CGFloat(size / 100)
        var paths = [UIBezierPath]()
        
        let getPath = drawShapes(shape: shape)
        
        switch count {
        case 1:
            paths.append(getPath(CGPoint(x: bounds.midX, y: bounds.midY), size))
        case 2:
            paths.append(getPath(CGPoint(x: bounds.midX-(size/4+spacing), y: bounds.midY), size))
            paths.append(getPath(CGPoint(x: bounds.midX+(size/4+spacing), y: bounds.midY), size))
        case 3:
            paths.append(getPath(CGPoint(x: bounds.midX-((size/4+spacing)*2), y: bounds.midY), size))
            paths.append(getPath(CGPoint(x: bounds.midX, y: bounds.midY), size))
            paths.append(getPath(CGPoint(x: bounds.midX+((size/4+spacing)*2), y: bounds.midY), size))
        default:
            break
        }
        return paths
    }
    
    private func drawShapes(shape: Shape) -> (CGPoint, CGFloat) -> UIBezierPath {
        var getPath: (CGPoint, CGFloat) -> UIBezierPath
        
        switch shape {
        case .diamond:  getPath = getDiamondPath
        case .oval:     getPath = getOvalPath
        case .squiggle: getPath = getSquigglePath
        }
        return getPath
    }
    
    // Drawing paths in cards
    private func drawPaths(for paths: [UIBezierPath]) {
        for path in paths {
            switch shading {
            case .outlined:
                path.lineWidth = 1.5
                color.setStroke()
                path.stroke()
            case .solid:
                color.setFill()
                path.fill()
            case .striped:
                path.lineWidth = 1.0
                color.setStroke()
                path.stroke()
                let context = UIGraphicsGetCurrentContext()
                context?.saveGState()
                path.addClip()
                path.lineWidth = 1.0
                for i in stride(from: 0, to: bounds.maxY, by: 7) {
                    path.move(to: CGPoint(x: 0, y: i))
                    path.addLine(to: CGPoint(x: bounds.maxX, y: i))
                }
                path.stroke()
                context?.restoreGState()
            }
        }
    }
    
    private func getDiamondPath(withCenter center: CGPoint, andSize size: CGFloat) -> UIBezierPath {
        let width = size / 4.5
        let halfHeight = size / 2
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: center.x, y: center.y - halfHeight))
        path.addLine(to: CGPoint(x: center.x + width, y: center.y))
        path.addLine(to: CGPoint(x: center.x, y: center.y + halfHeight))
        path.addLine(to: CGPoint(x: center.x - width, y: center.y))
        path.close()
        return path
    }
    
    private func getOvalPath(withCenter center: CGPoint, andSize size: CGFloat) -> UIBezierPath {
        let radius = size / 5
        let height = size / 4
        let path = UIBezierPath()
        
        path.addArc(withCenter: CGPoint(x: center.x, y: center.y+height), radius: radius, startAngle: 0, endAngle: CGFloat.pi, clockwise: true)
        path.addArc(withCenter: CGPoint(x: center.x, y: center.y-height), radius: radius, startAngle: CGFloat.pi, endAngle: 0, clockwise: true)
        path.close()
        return path
    }
    
    private func getSquigglePath(withCenter center: CGPoint, andSize size: CGFloat) -> UIBezierPath {
        let sf = size / 104
        let dx = center.x - (70*sf/2)
        let dy = center.y - (size/2)
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: 15*sf+dx, y: 104*sf+dy))
        path.addCurve(to: CGPoint(x: 54*sf+dx, y: 63*sf+dy), controlPoint1: CGPoint(x: 36.9*sf+dx, y: 112.4*sf+dy),
                      controlPoint2: CGPoint(x: 60.8*sf+dx, y: 89.7*sf+dy))
        path.addCurve(to: CGPoint(x: 53*sf+dx, y: 27*sf+dy), controlPoint1: CGPoint(x: 51.3*sf+dx, y: 52.3*sf+dy),
                      controlPoint2: CGPoint(x: 42*sf+dx, y: 42.2*sf+dy))
        path.addCurve(to: CGPoint(x: 40*sf+dx, y: 5*sf+dy), controlPoint1: CGPoint(x: 65.6*sf+dx, y: 9.6*sf+dy),
                      controlPoint2: CGPoint(x: 58.3*sf+dx, y: 5.4*sf+dy))
        path.addCurve(to: CGPoint(x: 12*sf+dx, y: 36*sf+dy), controlPoint1: CGPoint(x: 22*sf+dx, y: 4.6*sf+dy),
                      controlPoint2: CGPoint(x: 19.1*sf+dx, y: 9.7*sf+dy))
        path.addCurve(to: CGPoint(x: 14*sf+dx, y: 89*sf+dy), controlPoint1: CGPoint(x: 15.2*sf+dx, y: 59.2*sf+dy),
                      controlPoint2: CGPoint(x: 31.5*sf+dx, y: 61.9*sf+dy))
        path.addCurve(to: CGPoint(x: 15*sf+dx, y: 104*sf+dy), controlPoint1: CGPoint(x: 10*sf+dx, y: 95.3*sf+dy),
                      controlPoint2: CGPoint(x: 6.9*sf+dx, y: 100.9*sf+dy))
        return path
    }
}

//MARK: - Extensions
extension CardView: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = CardView()
        copy.color = color
        copy.count = count
        copy.shading = shading
        copy.shape = shape
        copy.isSelected = isSelected
        copy.bounds = bounds
        copy.frame = frame
        copy.alpha = 1
        return copy
    }
}
