//
//  EmojiThemes.swift
//  Concentration
//
//  Created by Dima Shelkov on 4/23/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class EmojiThemes {
    
    let themes = [
        "Halloween" : "🦇😱🎃👻😈🙀🍬🍭🍎👺👾",
        "Animals" : "🦓🦒🦔🦗🦝🦙🦛🦘🦡🦚🦢",
        "Sports" : "🏊‍♀️🏄‍♀️🤽‍♀️🚴‍♀️⛹️‍♀️🚣‍♀️🚵‍♀️🤾‍♀️🏊🏻‍♂️🚴‍♂️🤾‍♂️"
    ]
}
