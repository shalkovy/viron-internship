//
//  PlayingTableView.swift
//  setGame
//
//  Created by Dima Shelkov on 5/15/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class PlayingTableView: UIView {
    
    struct Constant {
        static let cellAspectRatio: CGFloat = 0.7
        static let spacingDx: CGFloat  = 3.0
        static let spacingDy: CGFloat  = 3.0
    }
    
    var cardViews = [CardView]()
    private var gridCards: Grid?
    var rowsGrid: Int { return gridCards?.dimensions.rowCount ?? 0 }

    override func layoutSubviews() {
        super.layoutSubviews()
        gridCards = Grid(
            layout: .aspectRatio(Constant.cellAspectRatio),
            frame: bounds
        )
        gridCards?.cellCount = cardViews.count
        layoutSetCards()
    }
    
    func layoutSetCards() {
        if let grid = gridCards {
            let columnsGrid = grid.dimensions.columnCount
            for row in 0..<rowsGrid {
                for column in 0..<columnsGrid {
                    if cardViews.count > (row * columnsGrid + column) {
                        UIViewPropertyAnimator.runningPropertyAnimator(
                            withDuration: 0.3,
                            delay:TimeInterval(row) * 0.1,
                            options: [.curveEaseInOut],
                            animations: {
                                self.cardViews[row * columnsGrid + column].frame =
                                    grid[row,column]!.insetBy(dx: Constant.spacingDx,
                                                              dy: Constant.spacingDy)})
                    }
                }
            }
        }
    }
    
    func addCardViews(newCardViews: [CardView]) {
        cardViews += newCardViews
        newCardViews.forEach { (cardView) in
            addSubview(cardView)
        }
        layoutIfNeeded()
    }
    
    func removeCardViews(removedCardViews: [CardView]) {
        removedCardViews.forEach{ (setCardView) in
            cardViews.remove(elements: [setCardView])
            setCardView.removeFromSuperview()
        }
        layoutIfNeeded()
    }
    
    func resetCards() {
        guard !cardViews.isEmpty else { return }
        
        for _ in cardViews.indices {
            let cardView = cardViews.removeFirst()
            cardView.removeFromSuperview()
        }
        layoutIfNeeded()
    }
}
