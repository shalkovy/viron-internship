//
//  SetViewController.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class SetViewController: UIViewController {

    //MARK: - Vars
    private var game = SetGame()
    private var matchedCards = [SetCard]()
    private var temporaryCards = [CardView]()
    private var roundedCards = [SetCard]()
    
    private var deckCenter: CGPoint {
        return view.convert(dealTreeCardsButton.center, to: playingTableView)
    }
    
    private var matchedCardViews: [CardView] {
        return playingTableView.cardViews.filter { $0.isMatched == true }
    }
    
    private var cardsCenter: CGPoint {
        return dealTreeCardsButton.convert(setsButton.center, to: playingTableView)
    }
    
    private var dealSetCardViews: [CardView] {
        return  playingTableView.cardViews.filter { $0.alpha == 0 }
    }
    
    // MARK: - Dynamic Animations vars
    lazy var animator = UIDynamicAnimator(referenceView: view)
    lazy var cardBehavior = CardBehavior(in: animator)
    
    //MARK: - Outlets
    @IBOutlet weak var setsButton: UIButton!
    @IBOutlet weak private var playingTableView: PlayingTableView!
    @IBOutlet weak var dealTreeCardsButton: UIButton!
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        game.generateFirstDeck()
        updateCardViewsFromModel()
        updateViewFromModel()
    }
    
    //MARK: - IBActions
    @IBAction private func addThreeCardsButtonPressed(_ sender: UIButton) {
        let _ = game.dealThreeCards()
        updateCardViewsFromModel()
        updateViewFromModel()
    }
    
    @IBAction private func newGameButtonPressed(_ sender: UIButton) {
        game = SetGame()
        game.generateFirstDeck()
        dealTreeCardsButton.isHidden = false
        updateViewFromModel()
    }
    
    //TODO: - rounded 
    @IBAction private func findSetButtonPressed(_ sender: UIButton) {
        roundedCards = game.findSet()
        updateViewFromModel()
        roundedCards.removeAll()
    }
    
    //MARK: - Functions
    private func addTapGestureRecognizer(for cardView: CardView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCard(recognizedBy:)))
        cardView.addGestureRecognizer(tap)
    }
    
    @objc private func tapOnCard(recognizedBy recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state,
            let cardView = recognizer.view as? CardView else { return }
        game.selectCard(addedIndex: playingTableView.cardViews.firstIndex(of: cardView)!)
        updateViewFromModel()
    }
    
    //MARK: - Updating View method
    private func updateViewFromModel() {
        updateCardViewsFromModel()
        
        guard game.mainDeck.isEmpty else { return }
        dealTreeCardsButton.isHidden = true
    }
    
    private func updateCardViewsFromModel() {
        var  newCardViews = [CardView]()
        
        if playingTableView.cardViews.count - game.cardsOnScreen.count > 0 {
            playingTableView.removeCardViews(removedCardViews: matchedCardViews)
        }
        
        let numberCardViews = playingTableView.cardViews.count
        
        for index in game.cardsOnScreen.indices {
            let card = game.cardsOnScreen[index]
            if  index > (numberCardViews - 1) {
                let cardView = CardView()
                updateCardView(cardView, from: card)
                
                cardView.alpha = 0
                addTapGestureRecognizer(for: cardView) 
                newCardViews += [cardView]
            } else {                                // old cards
                let cardView = playingTableView.cardViews[index]
                if cardView.alpha < 1 && cardView.alpha > 0 &&
                    !game.checkingForSetArray() {
                    cardView.alpha = 0
                }
                updateCardView(cardView, from: card)
            }
        }
        playingTableView.addCardViews(newCardViews: newCardViews)
        
        flyAwayAnimation()
        dealAnimation()
    }

    //MARK: - Animations
    private func dealAnimation () {
        let timeInterval = 0.15 * Double(playingTableView.rowsGrid + 1)
        dealTimer(withTimeInterval: timeInterval)
    }
    
    private func dealTimer(withTimeInterval timeInterval: Double) {
        var currentDealCard = 0
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { (timer) in
            for cardView in self.dealSetCardViews {
                cardView.animateDeal (
                     from: self.deckCenter,
                    delay: TimeInterval(currentDealCard) * 0.25)
                currentDealCard += 1
            }
        }
    }
    
    private func flyAwayAnimation() {
        let alreadyFliedCount =
            matchedCardViews.filter{($0.alpha < 1 && $0.alpha > 0)}.count
        
        if  game.checkingForSetArray(), alreadyFliedCount == 0 {
            temporaryCards.forEach { cardView in
                cardView.removeFromSuperview()
            }
            
            temporaryCards = []
            
            matchedCardViews.forEach { cardView in
                cardView.alpha = 0.2
                if let newCardView = cardView.copy() as? CardView {
                    temporaryCards += [newCardView]
                }
            }
            
            temporaryCards.forEach { cardView in
                playingTableView.addSubview(cardView)
                cardBehavior.addItem(cardView)
            }
            flyAwayTimer(temporaryCards: temporaryCards)
        }
    }
    
    private func flyAwayTimer(temporaryCards: [CardView]) {
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
            var j = 1
            for temporaryCard in self.temporaryCards {
                self.cardBehavior.removeItem(temporaryCard)
                temporaryCard.animateFly(to: self.cardsCenter,
                                         delay: TimeInterval(j) * 0.25)
                j += 1
            }
        }
    }
    
    // MARK: - Update cardViews
    private func updateCardView(_ cardView: CardView, from card: SetCard) {
        switch card.shape {
        case .squiggle: cardView.shape = .squiggle
        case .oval:     cardView.shape = .oval
        case .diamond:  cardView.shape = .diamond
        }
        
        switch card.shading {
        case .outlined: cardView.shading = .outlined
        case .solid:    cardView.shading = .solid
        case .striped:  cardView.shading = .striped
        }
        
        cardView.count = card.numberOfElements.rawValue
        
        switch card.color {
        case .red:    cardView.color = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        case .purple: cardView.color = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        case .green:  cardView.color = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
        
        cardView.isSelected = game.cardsSelected.contains(card)
        cardView.isRounded = roundedCards.contains(card)
        
        guard game.checkingForSetArray() else { return }
        cardView.isMatched = game.cardsTryMatched.contains(card)
    }
}
