//
//  ConcentrationThemeChooserViewController.swift
//  Concentration
//
//  Created by Dima Shelkov on 5/21/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {
    
    let emojiThemes = EmojiThemes()
    
    private var lastSeguedtToConcentrationViewController: ConcentrationViewController?
    private var concentrationViewController = ConcentrationViewController()
    
    private var splitViewDetailConcentrationViewController: ConcentrationViewController? {
        return splitViewController?.viewControllers.last as? ConcentrationViewController
    }

    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        if let cvc = secondaryViewController as? ConcentrationViewController {
            guard cvc.theme == nil else { return false }
        }
        return true
    }

    @IBAction func changeTheme(_ sender: Any) {
        if let cvc = splitViewDetailConcentrationViewController {
            if let themeName = (sender as? UIButton)?.currentTitle,
                let theme = emojiThemes.themes[themeName] {
                cvc.theme = theme
            }
        } else if let cvc = lastSeguedtToConcentrationViewController {
            if let themeName = (sender as? UIButton)?.currentTitle,
            let theme = emojiThemes.themes[themeName] {
                cvc.theme = theme
            }
            navigationController?.pushViewController(cvc, animated: true)
        } else if let cvc = storyboard?.instantiateViewController(withIdentifier: "Concentration") as? ConcentrationViewController {
            if let themeName = (sender as? UIButton)?.currentTitle {
                cvc.theme = emojiThemes.themes[themeName]
                self.navigationController?.pushViewController(cvc, animated: true)
            }
        }
    }
}
