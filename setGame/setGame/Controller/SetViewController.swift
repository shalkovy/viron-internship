//
//  SetViewController.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class SetViewController: UIViewController {
    
    @IBOutlet weak private var playingTableView: PlayingTableView! {
        didSet {
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(addThreeCardsButtonPressed(_:)))
            swipeDown.direction = .down
            playingTableView.addGestureRecognizer(swipeDown)
            
            let rotate = UIRotationGestureRecognizer(target: self, action: #selector(shuffleCardsOnTable))
            playingTableView.addGestureRecognizer(rotate)
        }
    }
    
    private var game = Set()
    private var cardsInSet = [SetCard]()
    @IBOutlet weak var dealTreeCardsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        game.generateFirstDeck()
        updateViewFromModel()
    }
    
    @IBAction private func addThreeCardsButtonPressed(_ sender: UIButton) {
        game.dealThreeCards()
        updateViewFromModel()
    }
    
    @IBAction private func newGameButtonPressed(_ sender: UIButton) {
        game = Set()
        game.generateFirstDeck()
        dealTreeCardsButton.isHidden = false
        updateViewFromModel()
    }
    
    @IBAction private func findSetButtonPressed(_ sender: UIButton) {
        guard cardsInSet.isEmpty else {
            cardsInSet.removeAll()
            updateViewFromModel()
            return
        }
        cardsInSet = game.findSet()
        updateViewFromModel()
    }
    
    private func addTapGestureRecognizer(for cardView: CardView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCard(recognizedBy: )))
        cardView.addGestureRecognizer(tap)
    }
    
    @objc private func tapOnCard(recognizedBy recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state,
            let cardView = recognizer.view as? CardView,
            let index = playingTableView.createdCardViews.firstIndex(of: cardView) else { return }
        cardView.selected = !cardView.selected
        game.addCardToChosenCardsArray(addedIndex: index)
        updateViewFromModel()
    }
    
    //MARK: - Shuffle cards method to rotate gesture
    @objc private func shuffleCardsOnTable() {
        game.cardsOnScreen.shuffle()
        updateViewFromModel()
    }
    
    //MARK: - Updating View method
    private func updateViewFromModel() {
        playingTableView.createdCardViews.removeAll()
        let cards = game.cardsOnScreen
        
        for index in cards.indices {
            guard let card = cards[index] else { return }
            let cardView = makeCardView(fromCard: card)
            addTapGestureRecognizer(for: cardView)
            playingTableView.createdCardViews.append(cardView)
            if cardsInSet.contains(card) {
                cardView.rounded = true
            }
            guard !game.chosenCards.contains(card) else { cardView.selected = true; continue }
        }
        guard game.mainDeck.isEmpty else { return }
        dealTreeCardsButton.isHidden = true
    }
    
    private func makeCardView(fromCard card: SetCard?) -> CardView {
        let cardView = CardView()
        guard let card = card else { return cardView }
        
        switch card.shape {
        case .squiggle: cardView.shape = .squiggle
        case .oval:     cardView.shape = .oval
        case .diamond:  cardView.shape = .diamond
        }
        
        switch card.shading {
        case .outlined: cardView.shading = .outlined
        case .solid:    cardView.shading = .solid
        case .striped:  cardView.shading = .striped
        }
        
        cardView.count = card.numberOfElements.rawValue
        switch card.color {
        case .red:    cardView.color = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        case .purple: cardView.color = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        case .green:  cardView.color = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
        return cardView
    }
}
