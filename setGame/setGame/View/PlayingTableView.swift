//
//  PlayingTableView.swift
//  setGame
//
//  Created by Dima Shelkov on 5/15/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

class PlayingTableView: UIView {
    
    var chosenCardViews = [CardView]()
    private var grid = Grid(layout: Grid.Layout.aspectRatio(2.5/3.5))
    var createdCardViews = [CardView]() {
        didSet { addSubviews() }
        willSet { removeSubviews() }
    }
    
    private func removeSubviews() {
        for card in createdCardViews {
            card.removeFromSuperview()
        }
    }
    
    private func addSubviews() {
        for card in createdCardViews {
            addSubview(card)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        grid.frame = bounds
        grid.cellCount = createdCardViews.count
        
        for index in createdCardViews.indices {
            let cardView = createdCardViews[index]
            cardView.frame = grid[index]!.zoom(by: 0.95)
            addSubview(createdCardViews[index])
        }
    }
}
