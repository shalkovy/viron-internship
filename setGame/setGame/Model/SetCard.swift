//
//  SetCard.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

struct SetCard: Equatable {
    
    let color: Color
    let shape: Shape
    let numberOfElements: Number
    let shading: Shading
    
    enum Color: String {
        case red = "red"
        case purple = "purple"
        case green = "green"
        
        static var all = [red, purple, green]
    }
    
    enum Shape: String {
        case oval = "oval"
        case squiggle = "squiggle"
        case diamond = "diamond"
        
        static var all = [oval, squiggle, diamond]
    }
    
    enum Number: Int {
        case one = 1
        case two = 2
        case three = 3
        
        static var all = [one, two, three]
    }
    
    enum Shading: String {
        case solid = "solid"
        case striped = "striped"
        case outlined = "outlined"
        
        static var all = [solid, striped, outlined]
    }
}
