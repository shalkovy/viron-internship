//
//  Set.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

class Set {
    
    var mainDeck = Deck().generatedArrayOfCards
    var cardsOnScreen = [SetCard?]()
    var chosenCards = [SetCard]()
    var chosenCardsIndices = [Int]()
    
    func dealThreeCards() {
        guard !mainDeck.isEmpty else { return }
        for _ in 0..<3 {
            cardsOnScreen.append(mainDeck.removeFirst())
        }
    }
    
    func generateFirstDeck() {
        for _ in 0..<12 {
            cardsOnScreen.append(mainDeck.removeFirst())
        }
    }
    
    // MARK: - touchButton Method
    func addCardToChosenCardsArray(addedIndex index: Int) {
        if chosenCardsIndices.count < 3 {
            chosenCards.inOut(element: cardsOnScreen[index]!)
            chosenCardsIndices.inOut(element: index)
        } else if chosenCardsIndices.count == 3 {
            guard checkingForSetArray(of: chosenCards) else { clean(); return }
            cardsOnScreen.removeAll { (card) -> Bool in
                if chosenCards.contains(card!) {
                    return true
                }
                return false
            }
            clean()
        }
    }
    
    func findSet() -> [SetCard] {
        var arrayOfCards = [SetCard]()
        for firstCard in cardsOnScreen {
            for secondCard in cardsOnScreen {
                for thirdCard in cardsOnScreen {
                    if firstCard != secondCard && secondCard != thirdCard, firstCard != nil && secondCard != nil && thirdCard != nil {
                        arrayOfCards.append(firstCard!)
                        arrayOfCards.append(secondCard!)
                        arrayOfCards.append(thirdCard!)
                    }
                    guard arrayOfCards.count == 3 else { continue }
                    if checkingForSetArray(of: arrayOfCards) {
                        return arrayOfCards
                    } else {
                        arrayOfCards.removeAll()
                    }
                }
            }
        }
        return [SetCard]()
    }
    
    // MARK: - Helping functions
    private func clean() {
        chosenCards.removeAll()
        chosenCardsIndices.removeAll()
    }
    
    // MARK: - Cheking Sets
    func checkingForSetArray(of arrayOfCards: [SetCard]) -> Bool {
        let cardOne = arrayOfCards[0], cardTwo = arrayOfCards[1], cardThree = arrayOfCards[2]
        if  formMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) &&
            colorMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) &&
            numberMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) &&
            textureMatched(firstCard: cardOne, secondCard: cardTwo, thirdCard: cardThree) {
            return true
        }
        return false
    }
    
    // MARK: - Matched or doesn't matched functions
    private func numberMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.numberOfElements == secondCard.numberOfElements && secondCard.numberOfElements == thirdCard.numberOfElements && firstCard.numberOfElements == thirdCard.numberOfElements {
            return true
        } else if firstCard.numberOfElements != secondCard.numberOfElements && secondCard.numberOfElements != thirdCard.numberOfElements && firstCard.numberOfElements != thirdCard.numberOfElements {
            return true
        }
        return false
    }
    
    private func colorMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.color == secondCard.color && secondCard.color == thirdCard.color && firstCard.color == thirdCard.color {
            return true
        } else if firstCard.color != secondCard.color && secondCard.color != thirdCard.color && firstCard.color != thirdCard.color {
            return true
        }
        return false
    }
    
    private func textureMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.shading == secondCard.shading && secondCard.shading == thirdCard.shading && firstCard.shading == thirdCard.shading {
            return true
        } else if firstCard.shading != secondCard.shading && secondCard.shading != thirdCard.shading && firstCard.shading != thirdCard.shading {
            return true
        }
        return false
    }
    
    private func formMatched(firstCard: SetCard, secondCard: SetCard, thirdCard: SetCard) -> Bool {
        if  firstCard.shape == secondCard.shape && secondCard.shape == thirdCard.shape && firstCard.shape == thirdCard.shape {
            return true
        } else if firstCard.shape != secondCard.shape && secondCard.shape != thirdCard.shape && firstCard.shape != thirdCard.shape {
            return true
        }
        return false
    }
}
