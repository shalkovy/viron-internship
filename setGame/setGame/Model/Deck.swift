//
//  File.swift
//  setGame
//
//  Created by Dima Shelkov on 4/24/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

class Deck {
    
    var generatedArrayOfCards = [SetCard]()
    
    init() {
        generatedArrayOfCards = generateDeck()
    }
    
    private func generateDeck() -> [SetCard] {
        for color in SetCard.Color.all {
            for shape in SetCard.Shape.all {
                for number in SetCard.Number.all {
                    for shading in SetCard.Shading.all {
                        let card = SetCard(color: color, shape: shape, numberOfElements: number, shading: shading)
                        generatedArrayOfCards.append(card)
                    }
                }
            }
        }
        return generatedArrayOfCards.shuffled()
    }
}


