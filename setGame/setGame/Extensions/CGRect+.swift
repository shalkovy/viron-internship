//
//  CGRect+.swift
//  setGame
//
//  Created by Dima Shelkov on 5/22/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import UIKit

extension CGRect {
    func zoom(by scale: CGFloat) -> CGRect {
        let newWidth = width * scale
        let newHeight = height * scale
        return insetBy(dx: (width - newWidth) / 2, dy: (height - newHeight) / 2)
    }
}
