//
//  Array+ .swift
//  setGame
//
//  Created by Dima Shelkov on 5/17/19.
//  Copyright © 2019 Dima Shelkov. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    mutating func inOut(element: Element) {
        guard let from = self.firstIndex(of: element) else {
            return self.append(element)
        }
        self.remove(at: from)
    }
    
    mutating func remove(elements: [Element]) {
        self = self.filter { !elements.contains($0) }
    }
}
